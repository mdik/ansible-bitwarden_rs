#!/bin/sh

cd /home/vaultwarden/vaultwarden/
docker-compose stop
docker-compose pull
docker-compose start
